import {Injectable} from '@angular/core';
import {CanLoad, Route, Router, UrlSegment} from '@angular/router';
import {Observable, of} from 'rxjs';
import {AuthService} from './auth.service';
import {switchMap, take} from 'rxjs/operators';
import {Location} from '@angular/common';
import {UserService} from '../service/user.service';
import {AngularFireAuth} from '@angular/fire/auth';

@Injectable({
    providedIn: 'root'
})
export class AuthGuard implements CanLoad {
    constructor(
        private router: Router,
        private authService: AuthService,
        private location: Location,
        private userService: UserService,
        private firebaseAuth: AngularFireAuth
    ) {
    }

    canLoad(
        route: Route,
        segments: UrlSegment[]
    ): Observable<boolean> | Promise<boolean> | boolean {
        return this.firebaseAuth.authState
            .pipe(
                take(1),
                switchMap(user => {
                    if (user == null) {
                        this.router.navigateByUrl('/auth');
                        return of(false);
                    }
                    return this.userService.getUserDetailsForAuthenticatedUser(user.uid)
                        .pipe(
                            take(1),
                            switchMap(userDetails => {
                                const requiredRole = route.data.role as string;
                                if (requiredRole === 'admin' && userDetails.roles.isAdmin) {
                                    return of(true);
                                }

                                if (requiredRole === 'client' && userDetails.roles.isClient) {
                                    return of(true);
                                }

                                if (requiredRole === 'employee' && userDetails.roles.isEmployee) {
                                    return of(true);
                                }

                                if (userDetails.roles.isAdmin) {
                                    this.router.navigateByUrl('admin');
                                    return of(false);
                                }
                                if (userDetails.roles.isEmployee) {
                                    this.router.navigateByUrl('employee');
                                    return of(false);
                                } else if (userDetails.roles.isClient) {
                                    this.router.navigateByUrl('client');
                                    return of(false);
                                }

                                this.location.back();
                                return of(false);
                            })
                        );
                })
            );
    }
}
