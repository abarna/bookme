import {Component, OnInit} from '@angular/core';
import {
    ModalController,
    AlertController,
    LoadingController
} from '@ionic/angular';
import {AuthService} from '../auth.service';
import {NgForm} from '@angular/forms';

@Component({
    selector: 'app-reset-credentials',
    templateUrl: './reset-credentials.component.html',
    styleUrls: ['./reset-credentials.component.scss']
})
export class ResetCredentialsComponent implements OnInit {
    isEmailInvalid = false;
    isPasswordInvalid = false;

    constructor(
        private modalCtrl: ModalController,
        private authService: AuthService,
        private alertCtrl: AlertController,
        private loadingCtrl: LoadingController
    ) {
    }

    ngOnInit() {
    }

    close() {
        this.modalCtrl.dismiss('test');
    }

    onSubmit(form: NgForm) {
        if (form.form.status === 'INVALID') {
            this.isEmailInvalid = true;
            return;
        }
        const email = form.form.value.email;

        this.loadingCtrl
            .create({
                keyboardClose: true,
                message: 'Trimitem email-ul pentru tine...'
            })
            .then(loadingEl => {
                loadingEl.present();
                this.authService.resetPassword(email).subscribe(
                    response => {
                        loadingEl.dismiss();
                        this.modalCtrl.dismiss(null, 'resetPassword');
                    },
                    err => {
                        loadingEl.dismiss();
                        if (err.error.error.message === 'INVALID_EMAIL') {
                            this.alertCtrl
                                .create({
                                    header: 'A aparut o eroare',
                                    message: 'Email-ul este invalid',
                                    buttons: ['Okay']
                                })
                                .then(alertEl => {
                                    alertEl.present();
                                });
                            return;
                        }
                        this.alertCtrl.create({
                            header: 'A aparut o eroare',
                            subHeader: 'Daca problema persista contactati administratorul',
                            buttons: ['OK']
                        })
                            .then((alertEl) => {
                                alertEl.present();
                            });
                    }
                );
            });
    }
}
