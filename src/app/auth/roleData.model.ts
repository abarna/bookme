export class RoleData {
  isAdmin: boolean;
  isClient: boolean;
  isEmployee: boolean;

  constructor(isAdmin: boolean, isEmployee: boolean, isClient: boolean) {
    this.isAdmin = isAdmin;
    this.isEmployee = isEmployee;
    this.isClient = isClient;
  }
}
