import {Injectable, OnDestroy} from '@angular/core';
import {BehaviorSubject, from, of} from 'rxjs';
import {User} from '../model/user.model';
import {environment} from '../../environments/environment';
import {map, tap} from 'rxjs/operators';
import {Plugins} from '@capacitor/core';
import {AdminEntity} from '../model/adminEntity.model';
import {AngularFireAuth} from '@angular/fire/auth';
import * as firebase from 'firebase';

export interface AuthResponseData {
    idToken: string;
    email: string;
    refreshToken: string;
    expiresIn: string;
    localId: string;
    registered?: boolean;
}

@Injectable({
    providedIn: 'root'
})
export class AuthService implements OnDestroy {
    private _user = new BehaviorSubject<User>(null);
    public userDetails = new BehaviorSubject<AdminEntity>(null);
    private activeLogoutTimer: any;
    private secondaryApp;

    constructor(private firebaseAuth: AngularFireAuth) {
    }

    ngOnDestroy() {
        if (this.activeLogoutTimer) {
            clearTimeout(this.activeLogoutTimer);
        }
    }

    createUser(email: string, password: string) {
        return this.firebaseAuth.createUserWithEmailAndPassword(email, password);
    }

    createEmployee(email: string, password: string) {
        if (this.secondaryApp === undefined) {
            this.secondaryApp = firebase.initializeApp(environment.firebaseConfig, 'Secondary');
        }
        const userCredentialPromise = this.secondaryApp.auth().createUserWithEmailAndPassword(email, password);
        this.secondaryApp.auth().signOut();
        return userCredentialPromise;
    }

    login(email: string, password: string) {
        return from(this.firebaseAuth.signInWithEmailAndPassword(email, password))
            .pipe(
                tap(userData => {
                    const tokenDuration = 3600;
                    const expirationTime = new Date(
                        new Date().getTime() + tokenDuration * 1000
                    );

                    const user = new User(
                        userData.user.uid,
                        userData.user.email,
                        userData.user.refreshToken,
                        expirationTime
                    );
                    this.storeAuthData(
                        userData.user.uid,
                        userData.user.email,
                        userData.user.refreshToken,
                        expirationTime.toISOString()
                    );
                    this._user.next(user);

                    // this will auto logout when the token will expire
                    this.logoutWhenTokenExpires(user.tokenDuration);
                })
            );

        // BELOW WAS AUTOLOGING  (saving on browser storage the data)
        // tap(userData => {
        //     const expirationTime = new Date(
        //         new Date().getTime() + +userData.expiresIn * 1000
        //     );
        //
        //     const user = new User(
        //         userData.localId,
        //         userData.email,
        //         userData.idToken,
        //         expirationTime
        //     );
        //     this.storeAuthData(
        //         userData.localId,
        //         userData.email,
        //         userData.idToken,
        //         expirationTime.toISOString()
        //     );
        //     this._user.next(user);
        //
        //     // this will auto logout when the token will expire
        //     this.logoutWhenTokenExpires(user.tokenDuration);
        // })
    }

    autoLogin() {
        return from(Plugins.Storage.get({key: 'authDataBookMe'})).pipe(
            map(storedData => {
                if (!storedData || !storedData.value) {
                    return null;
                }

                const parsedData = JSON.parse(storedData.value) as {
                    userId: string;
                    email: string;
                    token: string;
                    tokenExpirationDate: string;
                };

                const expirationDate = new Date(parsedData.tokenExpirationDate);
                if (expirationDate <= new Date()) {
                    Plugins.Storage.remove({key: 'authDataBookMe'});
                    return null;
                }
                const user = new User(
                    parsedData.userId,
                    parsedData.email,
                    parsedData.token,
                    expirationDate
                );

                return user;
            }),
            tap(user => {
                if (!user) {
                    return;
                }
                this._user.next(user);
                this.logoutWhenTokenExpires(user.tokenDuration);
            }),
            map(user => {
                return !!user;
            })
        );
    }

    logout() {
        return this.firebaseAuth.signOut()
            .then(() => {
                this._user.next(null);
                this.userDetails.next(null);
                Plugins.Storage.remove({key: 'authDataBookMe'});
            });
    }

    private logoutWhenTokenExpires(duration: number) {
        if (this.activeLogoutTimer) {
            clearTimeout(this.activeLogoutTimer);
        }
        this.activeLogoutTimer = setTimeout(() => {
            this.logout();
        }, duration);
    }

    resetPassword(userEmail: string) {
        // TODO implement it
        return of(null);
    }

    get userIsAuthenticated() {
        return this._user.asObservable().pipe(
            map(user => {
                if (user) {
                    return !!user.token;
                }
                return false;
            })
        );
    }

    get getAuthenticatedUserDetailsAsObservable() {
        return this.userDetails.asObservable();
    }

    get getUserIdAsObservable() {
        return this._user.asObservable().pipe(
            map(user => {
                if (user) {
                    return user.id;
                }
                return null;
            })
        );
    }

    private storeAuthData(
        userId: string,
        email: string,
        token: string,
        tokenExpirationDate: string
    ) {
        const data = JSON.stringify({
            userId,
            email,
            token,
            tokenExpirationDate
        });
        Plugins.Storage.set({
            key: 'authDataBookMe',
            value: data
        });
    }
}
