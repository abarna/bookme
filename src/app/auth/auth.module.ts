import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';

import {IonicModule} from '@ionic/angular';

import {AuthPageRoutingModule} from './auth-routing.module';

import {AuthPage} from './auth.page';
import {ResetCredentialsComponent} from './reset-credentials/reset-credentials.component';

@NgModule({
    imports: [CommonModule, FormsModule, IonicModule, AuthPageRoutingModule],
    declarations: [AuthPage, ResetCredentialsComponent],
    entryComponents: [ResetCredentialsComponent]
})
export class AuthPageModule {
}
