import {Component, OnInit} from '@angular/core';
import {NgForm} from '@angular/forms';
import {AuthService} from './auth.service';
import {AlertController, LoadingController, ModalController, ToastController} from '@ionic/angular';
import {tap} from 'rxjs/operators';
import {Router} from '@angular/router';
import {ResetCredentialsComponent} from './reset-credentials/reset-credentials.component';
import {AdminEntity} from '../model/adminEntity.model';
import {RoleData} from './roleData.model';
import {UserService} from '../service/user.service';
import {UserEntity} from '../model/userEntity.model';
import {AlertService} from '../service/alert.service';

@Component({
    selector: 'app-auth',
    templateUrl: './auth.page.html',
    styleUrls: ['./auth.page.scss']
})
export class AuthPage implements OnInit {
    isLogin = true;
    passwordsAreTheSame = true;

    constructor(
        private router: Router,
        private authService: AuthService,
        private loadingCtrl: LoadingController,
        private alertCtrl: AlertController,
        private modalCtrl: ModalController,
        private toastCtrl: ToastController,
        private userService: UserService,
        private alertService: AlertService
    ) {
    }

    ngOnInit() {
    }

    onSubmit(form: NgForm) {
        if (!form.valid) {
            return;
        }
        const email = form.value.email;
        const password = form.value.password;

        if (this.isLogin) {
            this.loadingCtrl
                .create({
                    keyboardClose: true,
                    message: 'Autentificare in curs...'
                })
                .then(loadingEl => {
                    loadingEl.present();
                    this.authService
                        .login(email, password)
                        .subscribe(
                            userCredentials => {
                                loadingEl.dismiss();
                                this.router.navigateByUrl('admin');
                                form.reset();
                            },
                            errResponse => {
                                loadingEl.dismiss();
                                const code = errResponse.code;
                                let message = 'A aparut o problema. Incercati mai tarziu';

                                if (code === 'auth/user-not-found' || code === 'auth/wrong-password') {
                                    message = 'Datele de conectare sunt incorecte...';
                                }
                                this.alertService.showAlert('Autentificare nereusita', message);
                            }
                        );
                });
        } else {
            // signup
            const confirmPassword = form.value.confirmPassword;
            const firstName = form.value.firstName;
            const lastName = form.value.lastName;
            const phone = form.value.phone;
            if (password !== confirmPassword) {
                this.passwordsAreTheSame = false;
            } else {
                this.passwordsAreTheSame = true;
                this.signUp(form, firstName, lastName, email, password, phone);
            }
        }
    }

    private signUp(form: NgForm, firstName: string, lastName: string,
                   email: string, password: string, phone: string) {
        this.loadingCtrl
            .create({
                keyboardClose: true,
                message: 'Creem contul pentru tine...'
            })
            .then(loadingEl => {
                loadingEl.present();
                this.authService
                    .createUser(email, password)
                    .then(
                        resData => {
                            return this.userService
                                .addUserDetail(
                                    new UserEntity(
                                        resData.user.uid,
                                        null,
                                        email,
                                        firstName,
                                        lastName,
                                        phone,
                                        new RoleData(false, false, true)
                                    )
                                );
                        },
                        err => {
                            const code = err.code;
                            let message = 'A aparut o problema. Incercati mai tarziu';

                            if (code === 'auth/user-not-found' || code === 'auth/wrong-password') {
                                message = 'Datele de conectare sunt incorecte...';
                            }
                            if (code === 'auth/email-already-in-use') {
                                message = 'Va rugam folositi alt email';
                            }
                            loadingEl.dismiss();
                            this.createAlert('Creare nereusita', message);
                        }
                    )
                    .then(result => {
                            if (result === undefined) {
                                return;
                            }
                            loadingEl.dismiss();
                            this.createAlert(
                                'Success',
                                'Contul a fost creat cu success'
                            );
                            form.reset();
                            this.isLogin = true;
                        },
                        err => {
                            loadingEl.dismiss();
                            this.createAlert(
                                'A aparut o eroare',
                                err.code
                            );
                        });
            });
    }

    onSwitchAuthMode(form: NgForm) {
        this.isLogin = !this.isLogin;
        if (this.isLogin) {
            this.passwordsAreTheSame = true;
        }
    }

    resetPassword() {
        this.modalCtrl
            .create({
                component: ResetCredentialsComponent
            })
            .then(modalEl => {
                modalEl.present();
                return modalEl.onDidDismiss();
            })
            .then(resulData => {
                if (resulData.role === 'resetPassword') {
                    this.toastCtrl
                        .create({
                            message: 'Verificati inbox-ul pentru a reseta parola.',
                            duration: 2000,
                            position: 'middle'
                        })
                        .then(toastEl => toastEl.present());
                }
            });
    }

    private createAlert(header: string, message: string) {
        this.alertCtrl
            .create({
                header,
                message,
                buttons: ['Okay']
            })
            .then(alertEl => alertEl.present());
    }
}
