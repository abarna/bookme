import {Component, OnInit} from '@angular/core';
import {LoadingController, MenuController} from '@ionic/angular';
import {LocationService} from '../service/location.service';
import {LocationModel} from '../model/location.model';
import {AlertService} from '../service/alert.service';

@Component({
    selector: 'app-client',
    templateUrl: './client.page.html',
    styleUrls: ['./client.page.scss']
})
export class ClientPage implements OnInit {
    locations: LocationModel[];

    constructor(private menuCtrl: MenuController,
                private locationService: LocationService,
                private  loadingCtrl: LoadingController,
                private alertService: AlertService) {
    }

    ngOnInit() {
    }

    ionViewDidEnter() {
        this.menuCtrl.enable(true, 'clientMenu');
        this.loadingCtrl.create({
            message: 'Cautam locatii pe placul tau'
        })
            .then(loadingEl => {
                loadingEl.present();
                this.locationService.getAllShops()
                    .subscribe(locationsResponse => {
                            this.locations = locationsResponse;
                            loadingEl.dismiss();
                        },
                        err => {
                            loadingEl.dismiss();
                            this.alertService.showAlert('A aparut o eroare', err);
                        });
            });
    }
}
