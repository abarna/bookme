import {Component, OnInit} from '@angular/core';
import {NgForm} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {AppointmentService} from '../../../service/appointment.service';
import {AppointmentEntity} from '../../../model/appointmentEntity.model';
import {AngularFireAuth} from '@angular/fire/auth';
import {UserEntity} from '../../../model/userEntity.model';
import {switchMap, take} from 'rxjs/operators';
import {UserService} from '../../../service/user.service';
import {AlertController, LoadingController} from '@ionic/angular';
import {ServiceType} from '../../../model/serviceType.enum';
import {AlertService} from '../../../service/alert.service';

@Component({
    selector: 'app-scheduler',
    templateUrl: './scheduler.page.html',
    styleUrls: ['./scheduler.page.scss'],
})
export class SchedulerPage implements OnInit {
    shopName: string;
    employeeFullName: string;
    employeeUuId: string;
    selectedDate;
    selectedHour;
    selectedService;
    monthValues = [6, 7, 8, 9];
    dayValues = [1, 2, 3, 4];
    yearValues = [2012, 2021];
    enableDayField;
    showHours;
    availableHours;
    authenticatedUser: UserEntity;
    minDateForUIDropDown;
    maxDateForUIDropDown;

    constructor(private route: ActivatedRoute,
                private appointmentService: AppointmentService,
                private firebaseAuth: AngularFireAuth,
                private userService: UserService,
                private loadingCtrl: LoadingController,
                private alertCtrl: AlertController,
                private alertService: AlertService,
                private router: Router) {
    }

    ngOnInit() {

    }

    ionViewDidEnter() {
        this.setMinMaxDatesForUI();
        this.route.paramMap.subscribe(paramMap => {
            if (paramMap.has('employeeId')) {
                this.employeeUuId = paramMap.get('employeeId');
            }
            if (paramMap.has('employeeFullName')) {
                this.employeeFullName = paramMap.get('employeeFullName');
                console.log(this.employeeFullName);
            }
            if (paramMap.has('shopName')) {
                this.shopName = paramMap.get('shopName');
                console.log(this.shopName);
            }
        });

        this.firebaseAuth.authState
            .pipe(
                take(1),
                switchMap(user => {
                    return this.userService.getUserDetailsForAuthenticatedUser(user.uid);
                })
            )
            .subscribe(userDetails => {
                this.authenticatedUser = userDetails;
            });
    }

    private setMinMaxDatesForUI() {
        const currentDate = new Date();
        let currentMonth;
        let currentDay;

        if (currentDate.getMonth() < 10) {
            currentMonth = '0' + (+currentDate.getMonth() + 1);
        } else {
            currentMonth = currentDate.getMonth() + 1;
        }

        if (currentDate.getDate() < 10) {
            currentDay = '0' + currentDate.getDate();
        } else {
            currentDay = currentDate.getDate();
        }
        this.minDateForUIDropDown = currentDate.getFullYear() + '-' + currentMonth + '-' + currentDay;
        this.maxDateForUIDropDown = currentDate.getFullYear() + 1;
    }

    onDesiredServiceChanged() {
        this.enableDayField = true;
        this.selectedDate = null;
        this.showHours = false;
    }

    onDesiredDayChanged() {
        this.selectedHour = null;

        if (this.selectedDate != null) {
            this.showHours = true;
            this.loadingCtrl.create({
                message: 'Cautam orele disponibile'
            })
                .then(loadingEl => {
                    loadingEl.present();
                    this.appointmentService.getAvailableHours(this.selectedService, this.employeeUuId,
                        new Date(this.selectedDate))
                        .subscribe(availablePeriods => {
                            this.availableHours = availablePeriods;
                            loadingEl.dismiss();
                        });
                });
        } else {
            this.showHours = false;
        }
    }

    onSelectHour(hour) {
        let serviceAsStringForAlertMessage;
        if (this.selectedService === ServiceType.TUNS) {
            serviceAsStringForAlertMessage = 'Tuns';
        } else if (this.selectedService === ServiceType.BARBA) {
            serviceAsStringForAlertMessage = 'Barba';
        } else if (this.selectedService === ServiceType.TUNSBARBA) {
            serviceAsStringForAlertMessage = 'Tuns si Barba';
        }
        this.alertCtrl.create({
            header: 'Esti sigur ca doresti rezervarea la ora ' + hour + ' pentru ' +
                serviceAsStringForAlertMessage + '?',
            buttons: [{
                text: 'Da',
                handler: value => {
                    this.loadingCtrl.create({
                        message: 'Pregatim rezervarea...'
                    }).then(loadingEl => {
                        loadingEl.present();

                        const appointmentDate = new Date(this.selectedDate);
                        appointmentDate.setHours(hour.split(':')[0]);
                        appointmentDate.setMinutes(hour.split(':')[1], 0, 0);
                        let duration = 30;
                        if (this.selectedService === ServiceType[ServiceType.TUNSBARBA]) {
                            duration = 60;
                        }
                        const appointmentEntity = new AppointmentEntity(
                            this.shopName,
                            this.employeeFullName,
                            this.employeeUuId,
                            this.authenticatedUser.userUuId,
                            appointmentDate.toISOString(),
                            duration,
                            this.authenticatedUser.firstName,
                            this.authenticatedUser.lastName,
                            this.authenticatedUser.phone,
                            this.authenticatedUser.email,
                            this.selectedService,
                            this.employeeUuId + appointmentDate.getFullYear()
                            + appointmentDate.getMonth() + appointmentDate.getDay()
                        );

                        this.appointmentService.addAppointment(appointmentEntity);
                        loadingEl.dismiss();
                        this.alertCtrl
                            .create({
                                header: 'Rezervarea a fost efectuata cu success',
                                subHeader: 'Multumim',
                                buttons: [
                                    {
                                        text: 'Ok',
                                        handler: buttonValue => {
                                            this.router.navigateByUrl('/client');
                                            this.selectedService = null;
                                            this.selectedDate = null;
                                        }
                                    }]
                            })
                            .then(alertEl => alertEl.present());

                    });
                }
            }, {
                text: 'Nu'
            }]
        }).then(alertEl => {
            alertEl.present();
        });
    }

    onSubmit(form: NgForm) {
    }
}
