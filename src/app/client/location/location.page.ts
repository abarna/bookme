import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {LocationService} from '../../service/location.service';
import {EmployeeEntity} from '../../model/employeeEntity.model';
import {LoadingController} from '@ionic/angular';
import {AlertService} from '../../service/alert.service';

@Component({
    selector: 'app-location',
    templateUrl: './location.page.html',
    styleUrls: ['./location.page.scss'],
})
export class LocationPage implements OnInit {
    locationId: string;
    shopName: string;
    ownerAndEmployeesOfTheShop: Array<EmployeeEntity> = [];

    constructor(private route: ActivatedRoute,
                private locationService: LocationService,
                private  loadingCtrl: LoadingController,
                private alertService: AlertService) {
    }

    ngOnInit() {
        this.route.paramMap.subscribe(paramMap => {
            if (paramMap.has('shopName')) {
                this.shopName = paramMap.get('shopName');
            }
            if (paramMap.has('shopId')) {
                this.locationId = paramMap.get('shopId');

                this.loadingCtrl.create({
                    message: 'Cautam angajatii disponibili'
                })
                    .then(loadingEl => {
                        loadingEl.present();
                        this.locationService.retrieveTheOwnerOfTheShop(this.locationId)
                            .subscribe(admin => {
                                    this.ownerAndEmployeesOfTheShop = [];
                                    this.ownerAndEmployeesOfTheShop.push(new EmployeeEntity(admin.userUuId, admin.userId,
                                        admin.email, admin.firstName, admin.lastName,
                                        admin.phone, admin.roles, admin.userId));
                                    this.locationService.retrieveTheEmployeesOfTheOwner(admin)
                                        .snapshotChanges()
                                        .subscribe(employees => {
                                            employees.map(employee => {
                                                const employeeEntity = employee.payload.val();
                                                employeeEntity.userId = employee.key;
                                                this.ownerAndEmployeesOfTheShop.push(employeeEntity);
                                            });
                                            loadingEl.dismiss();
                                        });
                                },
                                err => {
                                    this.alertService.showAlert('A aparut o eroare', err);
                                });
                    });
            } else {
                this.alertService.showAlert('A aparut o eroare', 'Incercati mai tarziu');
            }
        });
    }
}
