import {Component, OnInit} from '@angular/core';
import {switchMap, take} from 'rxjs/operators';
import {ActivatedRoute} from '@angular/router';
import {AppointmentService} from '../../service/appointment.service';
import {AngularFireAuth} from '@angular/fire/auth';
import {UserEntity} from '../../model/userEntity.model';
import {AppointmentEntityWithId} from '../../model/appointmentEntityWithId.model';
import {AlertService} from '../../service/alert.service';
import {AlertController, LoadingController} from '@ionic/angular';

@Component({
    selector: 'app-reservations',
    templateUrl: './reservations.page.html',
    styleUrls: ['./reservations.page.scss'],
})
export class ReservationsPage implements OnInit {

    authenticatedUser: UserEntity;
    appointments: AppointmentEntityWithId[];

    constructor(private route: ActivatedRoute,
                private appointmentService: AppointmentService,
                private firebaseAuth: AngularFireAuth,
                private alertService: AlertService,
                private alertCtrl: AlertController,
                private loadingCtrl: LoadingController) {
    }

    ngOnInit() {
    }

    ionViewDidEnter() {
        this.getCustomerAppointments();
    }

    private getCustomerAppointments() {
        this.firebaseAuth.authState
            .pipe(
                take(1),
                switchMap(user => {
                    return this.appointmentService.getClientAppointments(user.uid);
                })
            )
            .subscribe(appointments => {
                this.appointments = appointments.filter(appointment => {
                    return new Date(appointment.appointmentDate) > new Date();
                });
            });
    }

    cancelAppointment(id: string) {
        this.alertCtrl.create({
            header: 'Esti sigur?',
            buttons: [
                {
                    text: 'Da',
                    handler: value => {
                        this.loadingCtrl.create({
                            message: 'Stergem programarea pentru tine...'
                        })
                            .then(loadingEl => {
                                loadingEl.present();
                                this.appointmentService.deleteAppointment(id)
                                    .then(result => {
                                            this.alertService.showAlert('Success', 'Rezervarea a fost stearsa cu success');
                                            this.getCustomerAppointments();
                                            loadingEl.dismiss();
                                        },
                                        error => {
                                            this.alertService.showAlert('Eroare',
                                                'A aparut o eroare. Daca problema persista, ' +
                                                'va rugam sunati la locatie.');
                                            loadingEl.dismiss();
                                        });
                            });
                    }
                },
                {
                    text: 'Nu',
                    handler: value => {
                        console.log('Cancel delete appointment');
                    }
                }
            ]
        })
            .then(alertEl => {
                alertEl.present();
            });
    }
}
