import {NgModule} from '@angular/core';
import {PreloadAllModules, RouterModule, Routes} from '@angular/router';
import {AuthGuard} from './auth/auth.guard';

const routes: Routes = [
    {path: '', redirectTo: 'auth', pathMatch: 'full'},
    {
        path: 'auth',
        loadChildren: () => import('./auth/auth.module').then(m => m.AuthPageModule)
    },
    {
        path: 'admin',
        loadChildren: () =>
            import('./admin/admin.module').then(m => m.AdminPageModule),
        canLoad: [AuthGuard],
        data: {role: 'admin'}
    },
    {
        path: 'client',
        loadChildren: () =>
            import('./client/client.module').then(m => m.ClientPageModule),
        canLoad: [AuthGuard],
        data: {role: 'client'}
    },
    {
        path: 'employee',
        loadChildren: () =>
            import('./employee/employee.module').then(m => m.EmployeePageModule),
        canLoad: [AuthGuard],
        data: {role: 'employee'}
    }
];

@NgModule({
    imports: [
        RouterModule.forRoot(routes, {
            preloadingStrategy: PreloadAllModules
        })
    ],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
