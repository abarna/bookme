import {Component, Input, OnInit} from '@angular/core';
import {ModalController} from '@ionic/angular';
import {AppointmentService} from '../../service/appointment.service';
import {AlertService} from '../../service/alert.service';
import {AppointmentEntity} from '../../model/appointmentEntity.model';

@Component({
    selector: 'app-appointments-detail',
    templateUrl: './appointments-detail.component.html',
    styleUrls: ['./appointments-detail.component.scss'],
})
export class AppointmentsDetailComponent implements OnInit {

    @Input() appointmentId: string;
    appointment: AppointmentEntity;

    constructor(private modalCtrl: ModalController,
                private appointmentService: AppointmentService,
                private alertService: AlertService) {
    }

    ngOnInit() {
        console.log(this.appointmentId);
    }

    ionViewWillEnter() {
        this.appointmentService.getAppointmentDetails(this.appointmentId)
            .then(appointmentEntity => {
                this.appointment = appointmentEntity;
            })
            .catch(err =>
                this.alertService.showAlert('A aparut o eroare',
                    'Daca eroarea persista contacteaza administratorul')
            );
    }

    close() {
        this.modalCtrl.dismiss();
    }
}
