import {Component, Input, OnInit} from '@angular/core';
import {AppointmentsDetailComponent} from '../appointments-detail/appointments-detail.component';
import {AppointmentService} from '../../service/appointment.service';
import {ModalController} from '@ionic/angular';
import {AppointmentEntity} from '../../model/appointmentEntity.model';

@Component({
    selector: 'app-appointment',
    templateUrl: './appointment.component.html',
    styleUrls: ['./appointment.component.scss'],
})
export class AppointmentComponent implements OnInit {

    @Input() employeeUuid: string;
    appointments: AppointmentEntity[];

    constructor(private appointmentService: AppointmentService,
                private modalCtrl: ModalController) {
    }

    ngOnInit() {
        this.appointmentService.getEmployeesAppointments(this.employeeUuid)
            .subscribe(appointments => {
                this.appointments = appointments.filter(appointment => {
                    return new Date(appointment.appointmentDate) > new Date();
                });
            });
    }

    onAppointmentDetail(appointmentId: string) {
        this.modalCtrl
            .create({
                component: AppointmentsDetailComponent,
                componentProps: {
                    appointmentId
                }
            })
            .then(modalEl => {
                modalEl.present();
            });
    }

    close() {
        this.modalCtrl.dismiss();
    }
}
