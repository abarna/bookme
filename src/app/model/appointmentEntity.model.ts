import {ServiceType} from './serviceType.enum';

export class AppointmentEntity {

    constructor(
        public shopName: string,
        public employeeName: string,
        public employeeUuId: string,
        public clientUuid: string,
        public appointmentDate: string,
        public duration: number,
        public firstName: string,
        public lastName: string,
        public phone: string,
        public email: string,
        public serviceType: ServiceType,
        public employeeUuidAndDateWithoutHourFilterKey: string
    ) {
    }
}
