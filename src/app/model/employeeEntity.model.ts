import {RoleData} from '../auth/roleData.model';
import {AdminEntity} from './adminEntity.model';
import {UserEntity} from './userEntity.model';

export class EmployeeEntity extends UserEntity {

    constructor(public userUuId: string,
                public userId: string,
                public email: string,
                public firstName: string,
                public lastName: string,
                public phone: string,
                public roles: RoleData,
                public adminId: string,
               ) {
        super(userUuId, userId, email, firstName,
            lastName, phone, roles
        );
    }
}
