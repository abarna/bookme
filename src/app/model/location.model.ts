export class LocationModel {
    constructor(public shopName: string, public description: string,
                public shopId: string) {
    }
}
