import {RoleData} from '../auth/roleData.model';
import {UserEntity} from './userEntity.model';

export class AdminEntity extends UserEntity {

    constructor(
        public userUuId: string,
        public userId: string,
        public email: string,
        public firstName: string,
        public lastName: string,
        public phone: string,
        public roles: RoleData,
        public shopId?: string) {
        super(userUuId, userId, email, firstName,
            lastName, phone, roles);
    }

    public reset() {
        this.userUuId = null;
        this.userId = null;
        this.email = null;
        this.roles = null;
        this.shopId = null;
        this.firstName = null;
        this.lastName = null;
        this.phone = null;
    }
}
