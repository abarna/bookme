import {RoleData} from '../auth/roleData.model';

export class UserEntity {
    constructor(
        public userUuId: string,
        public userId: string,
        public email: string,
        public firstName: string,
        public lastName: string,
        public phone: string,
        public roles: RoleData) {
    }

    public reset() {
        this.userUuId = null;
        this.userId = null;
        this.email = null;
        this.roles = null;
        this.firstName = null;
        this.lastName = null;
        this.phone = null;
    }
}
