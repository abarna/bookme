import {AppointmentEntity} from './appointmentEntity.model';

export class AppointmentEntityWithId extends AppointmentEntity {

    constructor(
        public appointmentEntity: AppointmentEntity,
        public id?: string
    ) {
        super(appointmentEntity.shopName, appointmentEntity.employeeName,
            appointmentEntity.employeeUuId, appointmentEntity.clientUuid, appointmentEntity.appointmentDate,
            appointmentEntity.duration, appointmentEntity.firstName, appointmentEntity.lastName,
            appointmentEntity.phone, appointmentEntity.email, appointmentEntity.serviceType,
            appointmentEntity.employeeUuidAndDateWithoutHourFilterKey);
    }
}
