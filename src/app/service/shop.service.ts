import {Injectable} from '@angular/core';
import {ShopEntity} from '../model/shopEntity.model';
import {AuthService} from '../auth/auth.service';
import {BehaviorSubject, of} from 'rxjs';
import {AngularFireDatabase} from '@angular/fire/database';
import {AdminEntity} from '../model/adminEntity.model';
import {switchMap, take} from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})
export class ShopService {
    public shopSubject = new BehaviorSubject<ShopEntity>(null);

    constructor(
        private authService: AuthService,
        private firebaseDB: AngularFireDatabase
    ) {
    }

    public getShop(shopId: string) {
        const shopEntity = this.shopSubject.getValue();
        if (shopEntity) {
            return of(shopEntity);
        }

        return this.firebaseDB.list<ShopEntity>('shop', query => {
            return query.orderByKey().equalTo(shopId);
        })
            .snapshotChanges()
            .pipe(
                take(1),
                switchMap(returnedShopList => {
                    if (!returnedShopList) {
                        return of(new ShopEntity(null, null));
                    } else {
                        return of(new ShopEntity(
                            returnedShopList[0].payload.val().shopName,
                            returnedShopList[0].payload.val().description
                        ));
                    }
                })
            );
    }

    public updateShop(shopKey: string, shop: ShopEntity) {
        const observable = of(this.firebaseDB.database.ref('shop/' + shopKey)
            .update(shop));

        this.shopSubject.next(shop);
        return observable;
    }

    public saveShop(shop: ShopEntity) {
        const observable = of(this.firebaseDB.database.ref('shop')
            .push(shop));

        this.shopSubject.next(shop);
        return observable;
    }

    public updateAdminWithShopId(adminEntity: AdminEntity) {
        // creating admin entity without userId for not duplicating it in DB
        const adminDetails: AdminEntity = new AdminEntity(adminEntity.userUuId, null, adminEntity.email,
            adminEntity.firstName, adminEntity.lastName, adminEntity.phone, adminEntity.roles, adminEntity.shopId);
        const observable = of(this.firebaseDB.database.ref('users/' + adminEntity.userId)
            .update(adminDetails));
        return observable;
    }
}
