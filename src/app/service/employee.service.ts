import {Injectable} from '@angular/core';
import {BehaviorSubject} from 'rxjs';
import {map, switchMap, take} from 'rxjs/operators';
import {AuthService} from '../auth/auth.service';
import {EmployeeEntity} from '../model/employeeEntity.model';
import {AngularFireDatabase} from '@angular/fire/database';

@Injectable({
    providedIn: 'root'
})
export class EmployeeService {
    private employees = new BehaviorSubject<EmployeeEntity[]>(null);
    private employeesArray: EmployeeEntity[] = [];

    constructor(private authService: AuthService,
                private firebaseDB: AngularFireDatabase) {
    }

    public getEmployeesAsObservable() {
        return this.employees.asObservable();
    }

    public fetchEmployees(adminId: string) {
        return this.firebaseDB.list<EmployeeEntity>('users', query => {
            return query.orderByChild('adminId').equalTo(adminId);
        })
            .snapshotChanges()
            .pipe(
                take(1),
                map(employees => {
                    return employees.map(c =>
                        new EmployeeEntity(c.payload.val().userUuId, c.payload.key,
                            c.payload.val().email, c.payload.val().firstName, c.payload.val().lastName,
                            c.payload.val().phone, c.payload.val().roles, adminId)
                    );
                }),
                map(employees => {
                    this.employees.next(employees);
                    this.employeesArray = employees;
                    return employees;
                })
            );
    }

    public updateEmployeeDetail(employeeEntity: EmployeeEntity) {
        return this.firebaseDB.list<EmployeeEntity>('users', query => {
            return query.orderByKey().equalTo(employeeEntity.userId);
        })
            .snapshotChanges()
            .pipe(
                take(1),
                switchMap(userDetails => {
                    employeeEntity.userUuId = userDetails[0].payload.val().userUuId;
                    employeeEntity.roles = userDetails[0].payload.val().roles;
                    employeeEntity.email = userDetails[0].payload.val().email;
                    employeeEntity.adminId = userDetails[0].payload.val().adminId;
                    const employeeBodyForUpdate = Object.assign({}, employeeEntity);
                    // delete the key from body
                    delete employeeBodyForUpdate.userId;
                    return this.firebaseDB.database.ref('users/' + employeeEntity.userId)
                        .update(employeeBodyForUpdate);
                })
            );

    }

    public deleteEmployee(employeeId: string) {
        return this.firebaseDB.database.ref('users/' + employeeId)
            .remove()
            .then(response => {
                this.employeesArray = this.employeesArray
                    .filter(em => em.userId !== employeeId);
                this.employees.next(this.employeesArray);
                return response;
            });
    }
}
