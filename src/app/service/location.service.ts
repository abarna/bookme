import {Injectable} from '@angular/core';
import {LocationModel} from '../model/location.model';
import {AuthService} from '../auth/auth.service';
import {BehaviorSubject, Observable} from 'rxjs';
import {AngularFireDatabase} from '@angular/fire/database';
import {map, switchMap, take} from 'rxjs/operators';
import {ShopEntity} from '../model/shopEntity.model';
import {AdminEntity} from '../model/adminEntity.model';
import {EmployeeEntity} from '../model/employeeEntity.model';

@Injectable({
    providedIn: 'root'
})
export class LocationService {
    // TODO use this
    private locations = new BehaviorSubject<LocationModel[]>(null);
    private locationsArray = [];

    constructor(
        private authService: AuthService,
        private firebaseDB: AngularFireDatabase
    ) {
    }


    public getAllShops(): Observable<LocationModel[]> {
        return this.firebaseDB.list<ShopEntity>('shop')
            .snapshotChanges()
            .pipe(
                take(1),
                map(locations => {
                    const locationsArray: LocationModel[] = [];
                    for (const locationIndex in locations) {
                        const location: LocationModel = new LocationModel(
                            locations[locationIndex].payload.val().shopName,
                            locations[locationIndex].payload.val().description,
                            locations[locationIndex].payload.key,
                        );
                        locationsArray.push(location);
                    }
                    return locationsArray;
                })
            );
    }


    public retrieveTheOwnerOfTheShop(locationId: string) {
        return this.firebaseDB.list<AdminEntity>('users', query => {
            query.limitToFirst(1);
            return query.orderByChild('shopId').equalTo(locationId);
        })
            .snapshotChanges()
            .pipe(
                switchMap(admin => {
                    return admin.map(a => {
                        const adminEntity = a.payload.val();
                        adminEntity.userId = a.payload.key;
                        return adminEntity;
                    });
                })
            );
    }

    public retrieveTheEmployeesOfTheOwner(admin: AdminEntity) {
        return this.firebaseDB.list<EmployeeEntity>('users', query => {
            return query.orderByChild('adminId').equalTo(admin.userId);
        });
    }
}
