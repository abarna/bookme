import {Injectable} from '@angular/core';
import {AlertController} from '@ionic/angular';

@Injectable({
    providedIn: 'root'
})
export class AlertService {

    constructor(private alertCtrl: AlertController) {
    }

    showAlert(header: string, message: string) {
        this.alertCtrl
            .create({
                header,
                message,
                buttons: ['Ok']
            })
            .then(alertEl => alertEl.present());
    }
}
