import {Injectable} from '@angular/core';
import {AuthService} from '../auth/auth.service';
import {AdminEntity} from '../model/adminEntity.model';
import {Observable, of} from 'rxjs';
import {map} from 'rxjs/operators';
import {EmployeeEntity} from '../model/employeeEntity.model';
import {AngularFireDatabase} from '@angular/fire/database';
import {UserEntity} from '../model/userEntity.model';

@Injectable({
    providedIn: 'root'
})
export class UserService {

    constructor(
        private authService: AuthService,
        private firebaseDB: AngularFireDatabase
    ) {
    }

    public addEmployeeDetail(employeeEntity: EmployeeEntity) {
        return this.firebaseDB.database.ref('users')
            .push(employeeEntity);
    }

    public addUserDetail(adminEntity: UserEntity) {
        return this.firebaseDB.database.ref('users')
            .push(adminEntity);
    }

    public addAdminDetail(adminEntity: AdminEntity) {
        return this.firebaseDB.database.ref('users')
            .push(adminEntity);
    }

    public getUserDetailsForAuthenticatedUser(uid: string): Observable<AdminEntity> {
        if (this.authService.userDetails.getValue()) {
            return of(this.authService.userDetails.getValue());
        }

        return this.firebaseDB.list<AdminEntity>('users',
            ref => {
                ref.limitToFirst(1);
                return ref.orderByChild('userUuId').equalTo(uid);
            }).snapshotChanges()
            .pipe(
                map(customers => {
                    return customers.map(c => {
                            const adminDetails = new AdminEntity(c.payload.val().userUuId, c.payload.key,
                                c.payload.val().email, c.payload.val().firstName, c.payload.val().lastName,
                                c.payload.val().phone, c.payload.val().roles, c.payload.val().shopId);
                            this.authService.userDetails.next(adminDetails);
                            return adminDetails;
                        }
                    );
                }),
                map(customers => {
                    // always should return only one record
                    return customers[0];
                })
            );
    }
}
