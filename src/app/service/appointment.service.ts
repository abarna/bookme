import {Injectable} from '@angular/core';
import {AngularFireDatabase} from '@angular/fire/database';
import {AppointmentEntity} from '../model/appointmentEntity.model';
import {map, take} from 'rxjs/operators';
import {ServiceType} from '../model/serviceType.enum';
import {AppointmentEntityWithId} from '../model/appointmentEntityWithId.model';


@Injectable({
    providedIn: 'root'
})
export class AppointmentService {

    constructor(private firebaseDB: AngularFireDatabase) {
    }

    private getHalfAnHourPeriodsValues(openHour, closeHour) {
        const halfAnHourPeriods = [];
        let hourIterator;
        for (hourIterator = openHour; hourIterator < closeHour; hourIterator++) {
            let minutesIterator;
            for (minutesIterator = 0; minutesIterator < 2; minutesIterator++) {
                if (hourIterator < 10) {
                    if (minutesIterator % 2 === 0) {
                        halfAnHourPeriods.push('0' + hourIterator + ':00');
                    } else {
                        halfAnHourPeriods.push('0' + hourIterator + ':30');
                    }
                } else {
                    if (minutesIterator % 2 === 0) {
                        halfAnHourPeriods.push(hourIterator + ':00');
                    } else {
                        halfAnHourPeriods.push(hourIterator + ':30');
                    }
                }
            }
        }
        return halfAnHourPeriods;
    }

    public getAvailableHours(service: string, employeeUuId: string, selectedDate: Date) {
        return this.firebaseDB.list<AppointmentEntity>('appointments', query => {
            const filterValue = employeeUuId + +selectedDate.getFullYear()
                + selectedDate.getMonth() + selectedDate.getDay();
            return query.orderByChild('employeeUuidAndDateWithoutHourFilterKey').equalTo(filterValue);
        })
            .snapshotChanges()
            .pipe(
                take(1),
                map(
                    snapshot => snapshot.map(c => {
                            return c.payload.val();
                        }
                    )),
                map(appointments => {
                    const busyPeriods = [];
                    appointments.forEach(appointment => {
                        const aDate = new Date(appointment.appointmentDate);
                        let hour = (aDate.getHours()).toString();
                        let minutes = aDate.getMinutes().toString();
                        if (hour.length === 1) {
                            hour = '0' + hour;
                        }
                        if (minutes === '0') {
                            minutes = '00';
                        }
                        busyPeriods.push(hour + ':' + minutes);

                        if (appointment.serviceType === ServiceType.TUNSBARBA) {
                            // set next 30 min as busy
                            if (minutes === '00') {
                                minutes = '30';
                            } else {
                                minutes = '00';
                                hour = (+hour + 1).toString();
                                if (hour.length === 1) {
                                    hour = '0' + hour;
                                }
                            }
                            busyPeriods.push(hour + ':' + minutes);
                        }
                    });
                    let openHour = 8;
                    const currentDate = new Date();
                    if (selectedDate.getDay() === currentDate.getDay()) {
                        openHour = currentDate.getHours() + 1;
                    }
                    return this.getHalfAnHourPeriodsValues(openHour, 19)
                        .filter(
                            halfAnHourPeriod => {
                                return !busyPeriods.includes(halfAnHourPeriod);
                            }
                        );
                }));
    }

    public addAppointment(appointmentEntity: AppointmentEntity) {
        return this.firebaseDB.database.ref('appointments')
            .push(appointmentEntity);
    }

    public getEmployeesAppointments(employeeUuid: string) {
        return this.firebaseDB.list<AppointmentEntityWithId>('appointments', query => {
            return query.orderByChild('employeeUuId').equalTo(employeeUuid);
        })
            .snapshotChanges()
            .pipe(
                take(1),
                map(
                    snapshot => snapshot.map(c => {
                            return new AppointmentEntityWithId(c.payload.val(), c.payload.key);
                        }
                    ))
            );
    }

    public getClientAppointments(clientUuid: string) {
        return this.firebaseDB.list<AppointmentEntityWithId>('appointments', query => {
            return query.orderByChild('clientUuid').equalTo(clientUuid);
        })
            .snapshotChanges()
            .pipe(
                take(1),
                map(
                    snapshot => snapshot.map(c => {
                            return new AppointmentEntityWithId(c.payload.val(), c.payload.key);
                        }
                    ))
            );
    }

    public deleteAppointment(appointmentId: string) {
        return this.firebaseDB.database.ref('appointments/' + appointmentId)
            .remove();
    }

    public getAppointmentDetails(id: string): Promise<AppointmentEntity> {
        return this.firebaseDB.database.ref('appointments').child(id)
            .once('value')
            .then(snapshot => snapshot.val()
            );
    }
}
