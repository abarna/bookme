import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';

import {IonicModule} from '@ionic/angular';

import {AdminPageRoutingModule} from './admin-routing.module';

import {AdminPage} from './admin.page';
import {AppointmentComponent} from '../components/appointment/appointment.component';
import {AppointmentsDetailComponent} from '../components/appointments-detail/appointments-detail.component';

@NgModule({
    imports: [CommonModule, FormsModule, IonicModule, AdminPageRoutingModule],
    declarations: [AdminPage, AppointmentComponent, AppointmentsDetailComponent],
    entryComponents: [AppointmentComponent, AppointmentsDetailComponent]
})
export class AdminPageModule {
}
