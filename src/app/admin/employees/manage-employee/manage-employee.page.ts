import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {AuthService} from 'src/app/auth/auth.service';
import {switchMap, take} from 'rxjs/operators';
import {AlertController, LoadingController, ModalController} from '@ionic/angular';
import {RoleData} from 'src/app/auth/roleData.model';
import {UserService} from 'src/app/service/user.service';
import {EmployeeEntity} from 'src/app/model/employeeEntity.model';
import {AngularFireDatabase} from '@angular/fire/database';
import {AngularFireAuth} from '@angular/fire/auth';
import {EmployeeService} from '../../../service/employee.service';
import {AppointmentComponent} from '../../../components/appointment/appointment.component';
import {AlertService} from '../../../service/alert.service';

@Component({
    selector: 'app-manage-employee',
    templateUrl: './manage-employee.page.html',
    styleUrls: ['./manage-employee.page.scss']
})
export class ManageEmployeePage implements OnInit {
    isEditingEmployee = false;
    firstName = null;
    lastName = null;
    phone = null;
    email = null;
    password = null;
    form: FormGroup;
    employeeId: string;
    userUuId: string;
    private authenticatedUserId;

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private authService: AuthService,
        private employeeService: EmployeeService,
        private loadingCtrl: LoadingController,
        private alertCtrl: AlertController,
        private modalCtrl: ModalController,
        private userService: UserService,
        private firebaseDB: AngularFireDatabase,
        private firebaseAuth: AngularFireAuth,
        private alertService: AlertService
    ) {
    }

    ngOnInit() {
        this.route.paramMap.subscribe(paramMap => {
            if (paramMap.has('employeeId') && paramMap.has('email')
                && paramMap.has('firstName') && paramMap.has('lastName')
                && paramMap.has('phone') && paramMap.has('userUuId')) {
                this.employeeId = paramMap.get('employeeId');
                this.userUuId = paramMap.get('userUuId');
                this.email = paramMap.get('email');
                this.firstName = paramMap.get('firstName');
                this.lastName = paramMap.get('lastName');
                this.phone = paramMap.get('phone');
                this.password = 'fakePasswordForPassingTheFormValidation';
                this.isEditingEmployee = true;
            }
        });

        this.form = new FormGroup({
            firstName: new FormControl(this.firstName, {
                updateOn: 'blur',
                validators: [Validators.required]
            }),
            lastName: new FormControl(this.lastName, {
                updateOn: 'change',
                validators: [Validators.required]
            }),
            phone: new FormControl(this.phone, {
                updateOn: 'change',
                validators: [Validators.required]
            }),
            email: new FormControl(this.email, {
                updateOn: 'change',
                validators: [Validators.required, Validators.email]
            }),
            password: new FormControl(this.password, {
                updateOn: 'change',
                validators: [Validators.required, Validators.minLength(6)]
            })
        });
    }

    ionViewWillEnter() {
        this.authService.getUserIdAsObservable.pipe(take(1)).subscribe(userId => {
            this.authenticatedUserId = userId;
        });
    }

    onChangePassword() {
        // call service to change the password
    }

    addOrEdit() {
        if (this.isEditingEmployee) {
            // edit mode - password is not required
            this.alertCtrl.create({
                header: 'Esti sigur?',
                buttons: [
                    {
                        text: 'Da',
                        handler: value => {
                            this.loadingCtrl
                                .create({
                                    keyboardClose: true,
                                    message: 'Modificam datele angajatului...'
                                })
                                .then(loadingEl => {
                                    loadingEl.present();
                                    this.employeeService.updateEmployeeDetail(
                                        new EmployeeEntity(null, this.employeeId, null,
                                            this.form.value.firstName, this.form.value.lastName,
                                            this.form.value.phone, null, null)
                                    )
                                        .subscribe(response => {
                                                loadingEl.dismiss();
                                                this.alertService.showAlert('Datele au fost modificate', 'Operatie finalizata');
                                            },
                                            error => {
                                                loadingEl.dismiss();
                                                this.alertCtrl.create({
                                                    header: 'A aparut o eroare',
                                                    subHeader: 'Daca problema persista contactati administratorul',
                                                    buttons: ['OK']
                                                })
                                                    .then((alertEl) => {
                                                        alertEl.present();
                                                    });
                                            },);
                                });
                        }
                    },
                    {
                        text: 'Nu'
                    }
                ]
            })
                .then(alertEl => {
                    alertEl.present();
                });
        } else {
            // add mode - user & password are required
            if (!this.form.valid) {
                return;
            }

            let authenticatedUser;
            this.loadingCtrl
                .create({
                    keyboardClose: true,
                    message: 'Crearea contului este aproape gata...'
                })
                .then(loadingEl => {
                    loadingEl.present();
                    this.firebaseAuth.authState
                        .pipe(
                            switchMap(authenticated => {
                                    authenticatedUser = authenticated;
                                    return this.authService.createEmployee(this.form.value.email, this.form.value.password);
                                }
                            ),
                            switchMap(employee => {
                                return this.userService.getUserDetailsForAuthenticatedUser(authenticatedUser.uid)
                                    .pipe(
                                        switchMap(authenticatedUserDetails => {
                                                return this.userService
                                                    .addEmployeeDetail(
                                                        new EmployeeEntity(
                                                            // is ok but needs a ts fix
                                                            (employee as any).user.uid,
                                                            null,
                                                            this.form.value.email,
                                                            this.form.value.firstName,
                                                            this.form.value.lastName,
                                                            this.form.value.phone,
                                                            new RoleData(false, true, false),
                                                            authenticatedUserDetails.userId
                                                        )
                                                    );
                                            }
                                        )
                                    );
                            })
                        )
                        .subscribe(response => {
                                if (response === undefined) {
                                    return;
                                }
                                loadingEl.dismiss();
                                this.router.navigateByUrl('/admin');
                                this.form.reset();
                            },
                            err => {
                                loadingEl.dismiss();
                                const code = err.code;
                                let message = 'A aparut o problema la crearea angajatului..';

                                if (code === 'auth/email-already-in-use') {
                                    message = 'Email-ul este deja folosit!';
                                } else if (code === 'EMAIL_NOT_FOUND') {
                                    message = 'Email incorect...';
                                } else if (code === 'INVALID_PASSWORD') {
                                    message = 'Parola incorecta...';
                                } else if (code === 'INVALID_EMAIL') {
                                    message = 'Email-ul nu este valid';
                                }
                                this.showAlert('Inregistrare nereusita', message);
                            });
                });
        }
    }

    onGoToBookings() {
        this.modalCtrl
            .create({
                component: AppointmentComponent,
                componentProps: {
                    employeeUuid: this.userUuId
                }
            })
            .then(modalEl => {
                modalEl.present();
            });
    }

    showAlert(title: string, messageParam: string) {
        this.alertCtrl
            .create({
                header: title,
                message: messageParam,
                buttons: ['Okay']
            })
            .then(alertEl => alertEl.present());
    }
}
