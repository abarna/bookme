import {Component, OnInit} from '@angular/core';
import {EmployeeService} from '../../service/employee.service';
import {AlertController, IonItemSliding, LoadingController} from '@ionic/angular';
import {Router} from '@angular/router';
import {switchMap, take, tap} from 'rxjs/operators';
import {AuthService} from '../../auth/auth.service';
import {EmployeeEntity} from '../../model/employeeEntity.model';
import {UserService} from '../../service/user.service';
import {AngularFireAuth} from '@angular/fire/auth';

@Component({
    selector: 'app-employees',
    templateUrl: './employees.page.html',
    styleUrls: ['./employees.page.scss']
})
export class EmployeesPage implements OnInit {
    employees: EmployeeEntity[];

    constructor(
        private router: Router,
        private employeeService: EmployeeService,
        private authService: AuthService,
        private userService: UserService,
        private firebaseAuth: AngularFireAuth,
        private loadingCtrl: LoadingController,
        private alertCtrl: AlertController
    ) {
    }

    ngOnInit() {
    }

    ionViewWillEnter() {
        this.loadingCtrl
            .create({
                message: 'Cautam angajatii...'
            })
            .then(loadingEl => {
                loadingEl.present();
                return this.firebaseAuth.authState
                    .pipe(
                        take(1),
                        switchMap(user => {
                            return this.userService.getUserDetailsForAuthenticatedUser(user.uid);
                        }),
                        take(1),
                        switchMap(userDetails => {
                            return this.employeeService.fetchEmployees(userDetails.userId);
                        })
                    )
                    .subscribe(result => {
                            loadingEl.dismiss();
                        },
                        err => {
                            loadingEl.dismiss();
                            this.alertCtrl.create({
                                header: 'A aparut o eroare',
                                subHeader: 'Daca problema persista contactati administratorul',
                                buttons: ['OK']
                            })
                                .then((alertEl) => {
                                    alertEl.present();
                                });
                        });
            });

        this.employeeService.getEmployeesAsObservable()
            .subscribe(employees => {
                this.employees = employees;
            });
    }

    onEdit(userId: string, slidingEmployee: IonItemSliding) {
        const editEmployee = this.employees.filter(employee =>
            employee.userId === userId
        )[0];
        slidingEmployee.close();
        this.router.navigate([
            '/',
            'admin',
            'employees',
            'manage-employee',
            userId,
            editEmployee.email,
            editEmployee.firstName,
            editEmployee.lastName,
            editEmployee.phone,
            editEmployee.userUuId
        ]);
    }

    onDelete(employeeId: string, slidingEmployee: IonItemSliding) {
        this.alertCtrl.create({
            header: 'Esti sigur?',
            buttons: [
                {
                    text: 'Da',
                    handler: value => {
                        this.loadingCtrl.create({
                            message: 'Ne luam la revedere de la angajat...'
                        })
                            .then(loadingEl => {
                                loadingEl.present();
                                slidingEmployee.close();
                                this.employeeService.deleteEmployee(employeeId)
                                    .then(response => {
                                            loadingEl.dismiss();
                                        },
                                        err => {
                                            loadingEl.dismiss();
                                            this.alertCtrl.create({
                                                header: 'A aparut o eroare',
                                                subHeader: 'Daca problema persista contactati administratorul',
                                                buttons: ['OK']
                                            })
                                                .then((alertEl) => {
                                                    alertEl.present();
                                                });
                                        });
                            });
                    }
                },
                {
                    text: 'Nu'
                }]
        })
            .then(alertEl => {
                alertEl.present();
            });
    }
}
