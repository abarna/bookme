import {Component, OnInit} from '@angular/core';
import {MenuController, ModalController} from '@ionic/angular';
import {AuthService} from '../auth/auth.service';
import {Router} from '@angular/router';
import {AppointmentsDetailComponent} from '../components/appointments-detail/appointments-detail.component';
import {AppointmentComponent} from '../components/appointment/appointment.component';
import {AngularFireAuth} from '@angular/fire/auth';

@Component({
    selector: 'app-admin',
    templateUrl: './admin.page.html',
    styleUrls: ['./admin.page.scss']
})
export class AdminPage implements OnInit {
    constructor(
        private menuCtrl: MenuController,
        private authService: AuthService,
        private modalCtrl: ModalController,
        private router: Router,
        private firebaseAuth: AngularFireAuth
    ) {
    }

    private adminUuid;

    ngOnInit() {
    }

    ionViewDidEnter() {
        this.menuCtrl.enable(true, 'adminMenu');

        this.firebaseAuth.authState
            .subscribe(user => {
                this.adminUuid = user.uid;
            });
    }

    goToShop() {
        this.router.navigate(['/', 'admin', 'shop']);
    }

    showAppointments() {
        this.modalCtrl
            .create({
                component: AppointmentComponent,
                componentProps: {
                    employeeUuid: this.adminUuid
                }
            })
            .then(modalEl => {
                modalEl.present();
            });
    }
}
