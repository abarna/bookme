import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {ShopService} from 'src/app/service/shop.service';
import {ShopEntity} from 'src/app/model/shopEntity.model';
import {switchMap, take, tap} from 'rxjs/operators';
import {AlertController, LoadingController} from '@ionic/angular';
import {Router} from '@angular/router';
import {AuthService} from 'src/app/auth/auth.service';
import {AdminEntity} from '../../model/adminEntity.model';
import {AngularFireAuth} from '@angular/fire/auth';
import {UserService} from '../../service/user.service';
import {of} from 'rxjs';

@Component({
    selector: 'app-shop',
    templateUrl: './shop.page.html',
    styleUrls: ['./shop.page.scss']
})
export class ShopPage implements OnInit {
    private shop: ShopEntity;
    isLoading = false;
    form: FormGroup;
    authenticatedAdmin: AdminEntity;

    constructor(
        private shopService: ShopService,
        private loadingCtrl: LoadingController,
        private alertCtrl: AlertController,
        private router: Router,
        private authService: AuthService,
        private firebaseAuth: AngularFireAuth,
        private userService: UserService,
    ) {
    }

    ngOnInit() {
        this.isLoading = true;

        this.loadingCtrl
            .create({
                message: 'Incarcam detaliile magazinului...'
            })
            .then(loadingEl => {
                loadingEl.present();
                this.firebaseAuth.authState
                    .pipe(
                        take(1),
                        switchMap(user => {
                            return this.userService.getUserDetailsForAuthenticatedUser(user.uid);
                        }),
                        switchMap(userDetails => {
                            this.authenticatedAdmin = userDetails;
                            if (!userDetails.shopId) {
                                return of(null);
                            }
                            return this.shopService.getShop(userDetails.shopId);
                        })
                    )
                    .subscribe(returnedShop => {
                            if (!returnedShop) {
                                this.shop = new ShopEntity(null, null);
                            } else {
                                this.shop = returnedShop;
                            }

                            this.form = new FormGroup({
                                shopName: new FormControl(this.shop.shopName, {
                                    updateOn: 'change',
                                    validators: [Validators.required]
                                }),
                                shopDescription: new FormControl(this.shop.description, {
                                    updateOn: 'change',
                                    validators: [Validators.required, Validators.maxLength(180)]
                                }),
                                image: new FormControl(null)
                            });
                            this.isLoading = false;
                            loadingEl.dismiss();
                        },
                        error => {
                            loadingEl.dismiss();
                            this.alertCtrl.create({
                                header: 'A aparut o eroare',
                                subHeader: 'Daca problema persista contactati administratorul',
                                buttons: ['OK']
                            })
                                .then((alertEl) => {
                                    alertEl.present();
                                });
                        });
            });

    }

    saveShop() {
        this.loadingCtrl
            .create({
                message: 'Salvare in curs...'
            })
            .then(loadingEl => {
                loadingEl.present();

                if (this.authenticatedAdmin.shopId) {
                    // update shop
                    this.shopService
                        .updateShop(
                            this.authenticatedAdmin.shopId,
                            new ShopEntity(
                                this.form.value.shopName,
                                this.form.value.shopDescription
                            )
                        )
                        .subscribe(
                            res => {
                                loadingEl.dismiss();
                                this.router.navigateByUrl('/admin');
                            },
                            err => {
                                loadingEl.dismiss();
                                this.alertCtrl.create({
                                    header: 'A aparut o eroare',
                                    subHeader: 'Daca problema persista contactati administratorul',
                                    buttons: ['OK']
                                })
                                    .then((alertEl) => {
                                        alertEl.present();
                                    });
                            }
                        );
                } else {
                    // save shop
                    this.shopService
                        .saveShop(
                            new ShopEntity(
                                this.form.value.shopName,
                                this.form.value.shopDescription
                            )
                        )
                        .pipe(
                            tap(responseWithShopKeyInContent => {
                                this.authenticatedAdmin.shopId = (responseWithShopKeyInContent as any).key;
                                this.shopService
                                    .updateAdminWithShopId(this.authenticatedAdmin)
                                    .subscribe(
                                        shopKeyResponse => {
                                            loadingEl.dismiss();
                                            this.router.navigateByUrl('/admin');
                                        },
                                        err => {
                                            loadingEl.dismiss();
                                            this.createAlert('Avem o mica problema', err.error.error);
                                        }
                                    );
                            })
                        )
                        .subscribe(
                            responseWithShopKeyInContent => {
                                this.authService.getAuthenticatedUserDetailsAsObservable
                                    .pipe(
                                        take(1)
                                    )
                                    .subscribe(userDetails => {
                                        this.authService.userDetails.next(new AdminEntity(
                                            userDetails.userUuId,
                                            userDetails.userId,
                                            userDetails.email,
                                            userDetails.firstName,
                                            userDetails.lastName,
                                            userDetails.phone,
                                            userDetails.roles,
                                            (responseWithShopKeyInContent as any).key));
                                    });
                            },
                            err => {
                                loadingEl.dismiss();
                                this.alertCtrl.create({
                                    header: 'A aparut o eroare',
                                    subHeader: 'Daca problema persista contactati administratorul',
                                    buttons: ['OK']
                                })
                                    .then((alertEl) => {
                                        alertEl.present();
                                    });
                            }
                        );
                }
            });
    }

    private createAlert(header: string, message: string) {
        this.alertCtrl
            .create({
                header,
                message,
                buttons: ['Okay']
            })
            .then(alertEl => alertEl.present());
    }
}
