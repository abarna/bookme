import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {AlertController, MenuController, ModalController} from '@ionic/angular';
import {AppointmentEntity} from '../model/appointmentEntity.model';
import {AppointmentService} from '../service/appointment.service';
import {AngularFireAuth} from '@angular/fire/auth';
import {AppointmentsDetailComponent} from '../components/appointments-detail/appointments-detail.component';

@Component({
    selector: 'app-employee',
    templateUrl: './employee.page.html',
    styleUrls: ['./employee.page.scss']
})
export class EmployeePage implements OnInit {
    employeeUuid: string;
    appointments: AppointmentEntity[];

    constructor(
        private route: ActivatedRoute,
        private appointmentService: AppointmentService,
        private alertCtrl: AlertController,
        private modalCtrl: ModalController,
        private firebaseAuth: AngularFireAuth,
        private menuCtrl: MenuController
    ) {
    }

    ngOnInit() {
        this.route.paramMap.subscribe(paramMap => {
            // TODO change it to send employeeUuid not employeeId
            if (paramMap.has('employeeId')) {
                this.employeeUuid = paramMap.get('employeeId');
            } else {
                this.firebaseAuth.authState
                    .subscribe(user => {
                        this.employeeUuid = user.uid;
                    });
            }
        });
    }

    ionViewWillEnter() {
        this.menuCtrl.enable(true, 'employeeMenu');
        this.appointmentService.getEmployeesAppointments(this.employeeUuid)
            .subscribe(appointments => {
                this.appointments = appointments.filter(appointment => {
                    return new Date(appointment.appointmentDate) > new Date();
                });
            });
    }

    onAppointmentDetail(appointmentId: string) {
        this.modalCtrl
            .create({
                component: AppointmentsDetailComponent,
                componentProps: {
                    appointmentId: appointmentId
                }
            })
            .then(modalEl => {
                modalEl.present();
            });
    }
}
