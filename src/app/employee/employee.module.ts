import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';

import {IonicModule} from '@ionic/angular';

import {EmployeePageRoutingModule} from './employee-routing.module';

import {EmployeePage} from './employee.page';
import {AppointmentsDetailComponent} from '../components/appointments-detail/appointments-detail.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    EmployeePageRoutingModule
  ],
  declarations: [EmployeePage, AppointmentsDetailComponent],
  entryComponents: [AppointmentsDetailComponent]
})
export class EmployeePageModule {
}
