import {Component, OnInit, OnDestroy} from '@angular/core';

import {Platform} from '@ionic/angular';
import {AuthService} from './auth/auth.service';
import {Router} from '@angular/router';
import {Subscription} from 'rxjs';
import {Plugins, Capacitor, AppState} from '@capacitor/core';
import {take} from 'rxjs/operators';

@Component({
    selector: 'app-root',
    templateUrl: 'app.component.html',
    styleUrls: ['app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy {
    private authSub: Subscription;
    private previousAuthState = false;

    constructor(
        private platform: Platform,
        private authService: AuthService,
        private router: Router
    ) {
        this.initializeApp();
    }

    initializeApp() {
        this.platform.ready().then(() => {
            if (Capacitor.isPluginAvailable('SplashScreen')) {
                Plugins.SplashScreen.hide();
            }
        });
    }

    ngOnDestroy() {
        if (this.authSub) {
            this.authSub.unsubscribe();
        }
    }

    ngOnInit() {
        this.authSub = this.authService.userIsAuthenticated.subscribe(isAuth => {
            if (!isAuth && this.previousAuthState !== isAuth) {
                this.router.navigateByUrl('/auth');
            }
            this.previousAuthState = isAuth;
        });

        Plugins.App.addListener(
            'appStateChange',
            this.checkAuthOnResume.bind(this)
        );
    }

    goHome() {
        this.router.navigate(['/', 'client']);
    }

    checkReservations() {
        this.router.navigate(['/', 'client', 'reservations']);
    }

    logout() {
        this.authService.logout()
            .then(() => {
                this.router.navigate(['']);
                // reload the page after going to the main page to reactivate the guard
                location.reload();
            })
            .catch(err => {
                console.log('logout error ' + err);
            });
    }

    private checkAuthOnResume(state: AppState) {
        if (state.isActive) {
            this.authService
                .autoLogin()
                .pipe(take(1))
                .subscribe(success => {
                    if (!success) {
                        this.logout();
                    }
                });
        }
    }
}
