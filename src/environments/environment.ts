// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
    production: false,
    firebaseAPIKey: 'AIzaSyBDha48FmdbUKPfN26znGDdcZlydakRtwU',
    firebaseConfig: {
        apiKey: 'AIzaSyBDha48FmdbUKPfN26znGDdcZlydakRtwU',
        authDomain: 'bookme-7600c.firebaseapp.com',
        databaseURL: 'https://bookme-7600c.firebaseio.com',
        projectId: 'bookme-7600c',
        storageBucket: 'bookme-7600c.appspot.com',
        messagingSenderId: '539198097850',
        appId: '1:539198097850:web:6b894207a3e3ed3c86b583',
        measurementId: 'G-G72CNBGCRJ'
    }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
